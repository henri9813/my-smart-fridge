#!/usr/bin/env bash
set -e

setup

# Wait for composer vendor
if [ ! -f vendor/autoload.php ]; then
    echo "Run 'composer install' command to continue."
fi
while [ ! -f vendor/autoload.php ]; do
    sleep 1
done
echo "Composer vendor found, processing."

# Wait for application cache
if [ ! -f var/cache/dev/srcDevDebugProjectContainer.php ]; then
    echo "Run './bin/console cache:warmup' command to continue."
fi
while [ ! -f var/cache/dev/srcDevDebugProjectContainer.php ]; do
    sleep 1
done
echo "Application cache found, processing."

# Create log files if not exists to prevent permissions issue
mkdir -p var/log
touch var/log/dev.log
touch var/log/test.log
chown -R "${UNIX_USERNAME}:${UNIX_GROUP}" var/log

exec "$@"
