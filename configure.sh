#!/usr/bin/env bash

cp docker-compose.env.dist docker-compose.env

sed --in-place docker-compose.env --expression "s/{unix_username}/$(id -un)/"
sed --in-place docker-compose.env --expression "s/{unix_group}/$(id -gn)/"
sed --in-place docker-compose.env --expression "s/{unix_uid}/$(id -u)/"
sed --in-place docker-compose.env --expression "s/{unix_gid}/$(id -g)/"


##
## Parameters
##
if [ ! -e "docker-compose.override.yml" ]; then
    cp docker-compose.override.yml.dist docker-compose.override.yml
fi
if [ ! -e "config/parameters.yaml" ]; then
    cp config/parameters-dist.yaml config/parameters.yaml
fi

##
## Logs cleanup
##
rm --force var/log/*
