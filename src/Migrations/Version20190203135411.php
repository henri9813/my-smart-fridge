<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add user entity to the database
 */
final class Version20190203135411 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add user entity to the database';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(25) NOT NULL, last_name VARCHAR(25) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE access');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE access (id INT AUTO_INCREMENT NOT NULL, date DATETIME NOT NULL, url LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, referrer LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, user_agent VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, method VARCHAR(16) NOT NULL COLLATE utf8mb4_unicode_ci, domain VARCHAR(75) NOT NULL COLLATE utf8mb4_unicode_ci, status INT NOT NULL, ip VARCHAR(75) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX ip_idx (ip), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE user');
    }
}
