<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     *
     * @var string
     */
    private $name = "";

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FoodType", mappedBy="category", orphanRemoval=true)
     *
     * @var ArrayCollection
     */
    private $foodTypes;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->foodTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FoodType[]
     */
    public function getFoodTypes(): Collection
    {
        return $this->foodTypes;
    }

    public function addFoodType(FoodType $foodType): self
    {
        if (!$this->foodTypes->contains($foodType)) {
            $this->foodTypes[] = $foodType;
            $foodType->setCategory($this);
        }

        return $this;
    }

    public function removeFoodType(FoodType $foodType): self
    {
        if ($this->foodTypes->contains($foodType)) {
            $this->foodTypes->removeElement($foodType);

            // set the owning side to null (unless already changed)
            if ($foodType->getCategory() === $this) {
                $foodType->setCategory(null);
            }
        }

        return $this;
    }
}
