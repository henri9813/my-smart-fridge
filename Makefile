install:
	composer install
	yarn install

cs:
	php-cs-fixer fix --dry-run src/
	phpcs --standard=ruleset.xml
	yamllint . --strict --config-file .yamllint.yaml

cs-fix:
	php-cs-fixer fix .
	phpcbf . --standard=ruleset.xml


phpstan:
	php bin/console cache:clear --env=test
	# Have to use it locally because of namespace conflict.
	# @see https://github.com/phpstan/phpstan-shim/issues/12
	(vendor/bin/phpstan analyse --no-progress --no-ansi --errorFormat=raw -c phpstan.neon -l 7 src \
	 | sed 's#^/.*/\(tests\|src/\)#\1#g' | sort > .phpstan.lock || true) \
	 	&& git diff --exit-code .phpstan.lock

test: export RESET = 1
test: phpstan
	vendor/bin/phpunit --stop-on-failure $(PHPUNIT_FLAGS)

coverage: export RESET = 1
coverage: phpstan
	php -dzend_extension=xdebug.so vendor/bin/phpunit --coverage-text --colors=never --coverage-html coverage
